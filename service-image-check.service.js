
(function () {

    'use strict';

    function serviceImageCheck (
        $interval,
        $rootScope,
        $timeout
    ) {

        return {
            IsImageHeaderLoaded: function (imgSelector, scope) {

                $timeout(function () {
                    scope.showHeader = true;
                }, 0);

                var $this = this;

                var interval = $interval(function () {
                    if ($('.header-template').hasClass('no-image')) {

                        $rootScope.$emit('loadBlocks');
                        $rootScope.$emit('hidePreloader');
                    } else {

                        var img = document.querySelector(imgSelector);

                        if (img != undefined) {
                            var checkImageLoaded =  $this.checkImageLoaded(img, interval);
                            if (checkImageLoaded) {

                                $interval.cancel(interval);

                                $rootScope.$emit('loadBlocks');
                                $rootScope.$emit('hidePreloader');

                            }
                        }

                    }
                }, 100);
            },

            checkImageLoaded: function (img) {
                // During the onload event, IE correctly identifies any images that
                // weren’t downloaded as not complete. Others should too. Gecko-based
                // browsers act like NS4 in that they report this incorrectly.
                if (!img.complete) {
                    return false;
                }

                // However, they do have two very useful properties: naturalWidth and
                // naturalHeight. These give the true size of the image. If it failed
                // to load, either of these should be zero.
                if (img.naturalWidth === 0) {
                    return false;
                }

                // No other way of checking: assume it’s ok.
                return true;
            }
        }
    }

    serviceImageCheck.$inject = [
        '$interval',
        '$rootScope',
        '$timeout'
    ];

    angular
        .module('bravoureAngularApp')
        .factory('serviceImageCheck', serviceImageCheck);
})();

