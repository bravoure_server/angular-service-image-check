## Angular Service Image Check / Bravoure component

This Component is used to check if the Header images are loaded and after that load the blocks

### **Versions:**

1.0.2 - Updating timeout in showHeader option 
1.0.1 - Adding option to show the header 
1.0.0 - Initial Stable version

---------------------
